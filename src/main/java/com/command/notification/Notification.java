package com.command.notification;

public interface Notification {
    void notifyUser();
}
